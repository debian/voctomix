Source: voctomix
Section: video
Priority: optional
Maintainer: Holger Levsen <holger@debian.org>
Uploaders: Stefano Rivera <stefanor@debian.org>
Build-Depends: debhelper-compat (=13), dh-python, pandoc, python3:any
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/voc/voctomix
Vcs-Git: https://salsa.debian.org/debian/voctomix.git
Vcs-Browser: https://salsa.debian.org/debian/voctomix

Package: voctomix
Architecture: all
Depends:
 gstreamer1.0-tools,
 voctomix-core,
 voctomix-gui,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Suggests: fbset, gstreamer1.0-libav, python3-pyinotify, rlwrap
Description: Full-HD Software Live-Video-Mixer
 The Voctomix Project by the C3Voc (the Chaos Communication Congress' Video
 Operation Crew) is their software implementation of a Live-Video-Mixer.
 .
 It is written in Python using GStreamer and consists of three parts:
  - Voctocore, the videomixer core-process that does the actual video- and
    audio crunching.
  - Voctogui, a GUI implementation in GTK+ controlling the core's functionality
    and giving visual feedback of the mixed video.
  - Voctomix Example Scripts, a collection of tools and examples for talking
    to the core-process, feeding and receiving video-streams and controlling
    operations from scripts or command-line.
 .
 This package depends on all the components and also contains the
 documentation and example scripts.

Package: voctomix-core
Architecture: all
Depends:
 gir1.2-gst-plugins-base-1.0,
 gir1.2-gstreamer-1.0,
 gstreamer1.0-plugins-bad,
 gstreamer1.0-plugins-base,
 gstreamer1.0-plugins-good,
 gstreamer1.0-plugins-ugly,
 libgstreamer1.0-0,
 python3-gi,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Description: Full-HD Software Live-Video-Mixer (Core)
 The Voctomix Project by the C3Voc (the Chaos Communication Congress' Video
 Operation Crew) is their software implementation of a Live-Video-Mixer.
 .
 This package contains the core component, Voctocore, the videomixer
 core-process that does the actual video- and audio crunching.

Package: voctomix-gui
Architecture: all
Depends:
 gir1.2-gst-plugins-base-1.0,
 gir1.2-gstreamer-1.0,
 gir1.2-gtk-3.0,
 gstreamer1.0-alsa,
 gstreamer1.0-plugins-bad,
 gstreamer1.0-plugins-base,
 gstreamer1.0-plugins-good,
 gstreamer1.0-plugins-ugly,
 gstreamer1.0-tools,
 libgstreamer1.0-0,
 python3-gi,
 python3-gi-cairo,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Recommends: gstreamer1.0-gl
Suggests: gstreamer1.0-libav
Description: Full-HD Software Live-Video-Mixer (GUI)
 The Voctomix Project by the C3Voc (the Chaos Communication Congress' Video
 Operation Crew) is their software implementation of a Live-Video-Mixer.
 .
 This package contains the GUI component, Voctogui, a GUI implementation in
 GTK+ controlling the core's functionality.
